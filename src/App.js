import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import Landing from './components/Landing';
import SeriesPage from './components/SeriesPage';
import ActorPage from './components/ActorPage';

const App = (props) => (
    <BrowserRouter>
        <div className="App">
            <Route exact path="/" component={Landing} />
            <Route exact path="/serie/:serieId/actor/:id" component={ActorPage} />
            <Route exact path="/serie/:id" component={SeriesPage} />
        </div>
    </BrowserRouter>
);

export default App;
