import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ActorPersonalInfo.css';
import axios from 'axios';

class ActorPersonalInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        axios.get(`http://api.tvmaze.com/people/${this.props.id}`)
            .then(res => {
                this.setState({ actor: res.data });
            })
            .catch(error => {
                // Person not found
            });
    }

    render(){
        if(this.state.actor){
            const { name, gender, birthday, country, image } = this.state.actor;
            return (
                <div className="actor-info-container">
                    <div className="actor-info-photo-container actor-info-c-element">
                        <img src={(image !== null ? image.medium : '')} />
                    </div>
                    <div className="actor-text-container actor-info-c-element">
                        <span className="actor-info-c-name">{ name }</span>
                        <span className="actor-info-c-detail">{ gender }</span>
                        <span className="actor-info-c-detail">{ birthday }</span>
                        <span className="actor-info-c-detail">{ country.name }</span>
                    </div>
                </div>
            );
        }

        return (
            <div className="actor-info-container" />
        );
    }
}

ActorPersonalInfo.defaultProps = {
    id: '1'
};

ActorPersonalInfo.propTypes = {
    id: PropTypes.string.isRequired
};

export default ActorPersonalInfo;