import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Toast.css';

class Toast extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: true
        };
    }

    componentDidMount() {
        clearInterval(this.showTimer);
        clearInterval(this.hideTimer);
        this.showTimer = setInterval(this.fadeOut.bind(this), 1000);
    }

    fadeOut() {
        clearInterval(this.showTimer);
        clearInterval(this.hideTimer);
        this.setState({ show: false });
        this.hideTimer = setInterval(this.hideMessage.bind(this), 1000);
    }

    hideMessage() {
        clearInterval(this.showTimer);
        clearInterval(this.hideTimer);
        this.props.hideMessage();
    }

    render() {
        const className = `toast ${ (this.props.toast.positive) ? 'toast-success' : 'toast-error' } ${(this.state.show) ? 'fadeIn' : 'fadeOut'}`;

        return (
            <div className={className}>
                { this.props.toast.message }
            </div>
        );
    }


}

Toast.defaultProps = {
    toast: {
        message: '',
        positive: true
    },
    hideMessage: (positive) => alert('hideMessage')
};

Toast.propTypes = {
    toast: PropTypes.shape({
        message: PropTypes.string.isRequired,
        positive: PropTypes.bool.isRequired
    }).isRequired,
    hideMessage: PropTypes.func.isRequired
};

export default Toast;