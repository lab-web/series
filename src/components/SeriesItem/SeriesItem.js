import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './SeriesItem.css';
import axios from 'axios';
import ActorList from '../ActorList';
import {NavLink} from 'react-router-dom';

class SeriesItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            actors: [],
            seriesFound: true
        };
        this.onDeleteClicked = this.onDeleteClicked.bind(this);
    }

    componentWillMount() {
        axios.get(`http://api.tvmaze.com/shows/${this.props.id}/cast`)
            .then(res => {
                this.setState({actors: res.data});
            })
            .catch(error => {
                // Series not found
                this.setState({seriesFound: false});
            });
    }

    onDeleteClicked() {
        this.props.onDeleteSeries(this.props.id);
    }

    renderActors() {
        if (this.state.seriesFound) {
            return (
                <ActorList actors={this.state.actors} fromSeries={this.props.id}/>
            );
        }

        return (
            <div className="no-actors-container">
                <p className="no-actors-p">Sin Actores</p>
            </div>
        );
    }

    renderTitle() {
        const {name, id} = this.props;
        if (this.state.seriesFound) {
            return (
                <NavLink to={`/serie/${this.props.id}`} className="series-title-anchor">
                    <p className="series-title-container">
                        <span className="series-title">{ name }</span>
                        <span className="series-id">{ id }</span>
                    </p>
                </NavLink>
            );
        } else {
            return (
                <p className="series-title-container">
                    <span className="series-title">{ name }</span>
                    <span className="series-id">{ id }</span>
                </p>
            );
        }
    }

    render() {
        const {seriesFound} = this.state;
        return (
            <div className={"series-container " + ((seriesFound) ? "series-found" : "series-not-found")}>
                <div className="series-delete-button-container">
                    <button className="delete-button" onClick={this.onDeleteClicked}>X</button>
                </div>
                <div className="series-item">
                    {this.renderTitle()}
                </div>
                <div className="series-actors">
                    {this.renderActors()}
                </div>
            </div>
        );
    }
}

SeriesItem.defaultProps = {
    id: 0,
    name: '[nombre]',
    onDeleteSeries: (id) => alert('onDeleteSeries')
};

SeriesItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    onDeleteSeries: PropTypes.func.isRequired
};

export default SeriesItem;