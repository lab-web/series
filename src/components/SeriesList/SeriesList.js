import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SeriesList.css';
import SeriesItem from '../SeriesItem';

class SeriesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            toast: {
                message: '',
                positive: true
            }
        };
    }

    renderSeries() {
        const { series } = this.props;
        return series.map( seriesItem => {
            const { id, name } = seriesItem;
            return (
                <SeriesItem
                    key={id}
                    id={id}
                    name={name}
                    onDeleteSeries={this.props.onDeleteSeries} />
            );
        });
    }

    render() {
        return (
            <div className="series-list">
                <div className="list-elements-container">
                    { this.renderSeries() }
                </div>
            </div>
        );
    }
}

SeriesList.defaultProps = {
    series: [],
    onDeleteSeries: (id) => alert('onDeleteSeries')
};

SeriesList.propTypes = {
    series: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    onDeleteSeries: PropTypes.func.isRequired
};

export default SeriesList;