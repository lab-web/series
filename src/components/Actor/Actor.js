import React from 'react';
import PropTypes from 'prop-types';
import './Actor.css';

const Actor = ({ actor: { person, character}}) => (
    <li>
        <div className="actor-element-container">
            <div className="actor-photo actor-info-element">
                <img src={(character.image !== null ? character.image.medium : '')}  />
            </div>
            <div className="actor-info-element actor-text-elements">
                <div>
                    <span className="actor-name-text">{person.name}</span>
                </div>
                <div>
                    <span className="actor-character-text">{character.name}</span>
                </div>
            </div>
        </div>
    </li>
);

Actor.defaultProps = {
    actor: {
        person: {
            name: '[nombre]'
        },
        character: {
            name: '[personaje]',
            image: {
                medium: '[imagen]'
            }
        }
    }
};

Actor.propTypes = {
    actor: PropTypes.shape({
        person: PropTypes.shape({
            name: PropTypes.string.isRequired
        }).isRequired,
        character: PropTypes.shape({
            name: PropTypes.string.isRequired,
            image: PropTypes.shape({
                medium: PropTypes.string
            })
        }).isRequired
    }).isRequired
};

export default Actor;