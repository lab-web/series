import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AddSeriesForm.css';

class AddSeriesForm extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    submit(e) {
        const { _name, _id } = this.refs;
        e.preventDefault();
        if(seriesTextValidator(_name.value) && seriesIdValidator(_id.value)) {
            this.props.onNewSeries(_name.value, parseInt(_id.value.trim()));
            _name.value = '';
            _id.value = '';
        } else {
            if(!seriesTextValidator(_name.value) && !seriesIdValidator(_id.value)) {
                this.props.showMessage('Nombre debe estar entre 1 y 100 caracteres y el id debe ser numérico', false);
            } else if(!seriesTextValidator(_name.value) ){
                this.props.showMessage('Nombre debe estar entre 1 y 100 caracteres', false);
            } else {
                this.props.showMessage('El id debe ser numérico', false);
            }
        }
    }

    render() {
        return (
            <div>
                <div className="series-form-container">
                    <div id="form-title-wrapper">
                        <span className="form-title">Agregar Serie</span>
                    </div>
                    <div>
                        <form onSubmit={this.submit}>
                            <input ref="_name" type="text" placeholder="Nombre de la serie" className="series-form-input"/>
                            <input ref="_id" type="text" placeholder="Id de la serie" className="series-form-input"/>
                            <button className="beautiful-button">Agregar</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const seriesTextValidator = (value) => {
    return (typeof value === 'string' && value.trim().length >= 1 && value.trim().length <= 100);
};

const seriesIdValidator = (value) => {
    let reg = /^\d+$/;
    return (typeof value === 'string' && value.trim().length >= 1 && reg.test(value.trim()));
};

AddSeriesForm.defaultProps = {
    onNewSeries: (name, id) => alert('onNewSeries'),
    showMessage: (message, positive) => alert(`Should showMessage(${message}, ${positive})`)
};

AddSeriesForm.propTypes = {
    onNewSeries: PropTypes.func.isRequired,
    showMessage: PropTypes.func.isRequired
};

export default AddSeriesForm;