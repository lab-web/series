import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ActorList.css';
import Actor from '../Actor';
import { NavLink } from 'react-router-dom';

class ActorList extends Component {

    renderActors() {
        const { actors } = this.props;
        return actors.map( actorItem => {
            const { person, character } = actorItem;
            return (
                <NavLink to={`/serie/${this.props.fromSeries}/actor/${person.id}`} key={character.id}>
                    <Actor actor={actorItem} />
                </NavLink>
            );
        });
    }

    render() {
        return (
            <div className="actor-list-container">
                <ul className="actor-list-ul">
                    { this.renderActors() }
                </ul>
            </div>
        );
    }
}

ActorList.defaultProps = {
    actors: [],
    fromSeries: 1
};

ActorList.propTypes = {
    actors: PropTypes.arrayOf(PropTypes.shape({
        person: PropTypes.shape({
            name: PropTypes.string.isRequired
        }).isRequired,
        character: PropTypes.shape({
            name: PropTypes.string.isRequired,
            image: PropTypes.shape({
                medium: PropTypes.string.isRequired
            }).isRequired
        }).isRequired
    })).isRequired,
    fromSeries: PropTypes.number.isRequired
};

export default ActorList;