import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './ActorCatalogue.css';
import axios from 'axios';
import { NavLink } from 'react-router-dom';
import Actor from '../Actor';

class ActorCatalogue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            actors: [],
            seriesFound: true
        };
    }

    componentWillMount() {
        axios.get(`http://api.tvmaze.com/shows/${this.props.id}/cast`)
            .then(res => {
                this.setState({ actors: res.data });
            })
            .catch(error => {
                // Series not found
                this.setState({ seriesFound: false });
            });
    }

    renderActors() {
        const { actors } = this.state;
        return actors.map( actorItem => {
            const { person, character } = actorItem;
            return (
                <NavLink to={`/serie/${this.props.id}/actor/${person.id}`} key={character.id} >
                    <Actor key={character.id} actor={actorItem} />
                </NavLink>
            );
        });
    }

    render() {
        return (
            <div className="detail-page">
                <ul className="catalogue-list">
                    { this.renderActors() }
                </ul>
            </div>
        );
    }
}

ActorCatalogue.defaultProps = {
    id: '1'
};

ActorCatalogue.propTypes = {
    id: PropTypes.string.isRequired
};

export default ActorCatalogue;