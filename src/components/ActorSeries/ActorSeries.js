import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ActorSeries.css';
import axios from 'axios';

class ActorSeries extends Component {

    constructor(props){
        super(props);
        this.state = {
            series: []
        };
    }

    componentWillMount(){
        axios.get(`http://api.tvmaze.com/people/${this.props.id}/castcredits?embed=show`)
            .then(res => {
                this.setState({ series: res.data });
            })
            .catch(error => {
                // Person not found
            });
    }

    renderActorSeries(){
        return this.state.series.map(({ _embedded: { show: { id, name, premiered } } }) => (
            <li className="actor-series-element-li" key={id}>
                <span>{`${(premiered !== null) ? premiered.substring(0,4) : '[no premier date]'} - ${ name }`}</span>
            </li>
        ));
    }

    render(){
        return (
            <div className="actor-series-container-list">
                <p>Series</p>
                <ul>
                    { this.renderActorSeries() }
                </ul>
            </div>
        );
    }
}

ActorSeries.defaultProps = {
    id: '1'
};

ActorSeries.propTypes = {
    id: PropTypes.string.isRequired
};

export default ActorSeries;