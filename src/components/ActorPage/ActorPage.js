import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import './ActorPage.css';
import Header from '../Header';
import ActorPersonalInfo from '../ActorPersonalInfo';
import ActorSeries from '../ActorSeries';

class ActorPage extends Component {

    constructor(props) {
        super(props);
        let seriesName = '[series]';
        const localStorageSeries = JSON.parse(localStorage.getItem('series'));
        if(Array.isArray(localStorageSeries)) {
            const seriesFiltered = localStorageSeries.filter(series => (series.id === parseInt(this.props.match.params.serieId) ));
            if(seriesFiltered.length > 0) {
                seriesName = `${seriesFiltered[0].name}`;
            }
        }
        this.state = { seriesName, actorName: '[actor]' }
    }

    componentWillMount() {
        axios.get(`http://api.tvmaze.com/people/${this.props.match.params.id}`)
            .then(res => {
                this.setState({ actorName: res.data.name });
            })
            .catch(error => {
                // Person not found
            });
    }

    render() {
        const { seriesName, actorName } = this.state;
        return (
            <div>
                <Header title={ `${actorName} / ${seriesName}` } />
                <div className="actor-page-content">
                    <div className="actor-page-elements-container">
                        <ActorPersonalInfo id={this.props.match.params.id} />
                        <ActorSeries id={this.props.match.params.id} />
                    </div>
                </div>
            </div>
        );
    }
}

ActorPage.defaultProps = {
    match: {
        params: {
            id: '1',
            serieId: '1'
        }
    }
};

ActorPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string.isRequired,
            serieId: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
};

export default ActorPage;