import React from 'react';
import './Header.css';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const Header = (props) => (
    <div className="header-container">
        <span className="header-link">
            <NavLink to="/">REGRESAR</NavLink>
        </span>
        <span className="header-title">
            { props.title }
        </span>
    </div>
);

Header.defaultProps = {
    title: '[title]'
};

Header.propTypes = {
    title: PropTypes.string.isRequired
};

export default Header;