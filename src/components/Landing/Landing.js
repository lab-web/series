import React, { Component } from 'react';
import AddSeriesForm from '../AddSeriesForm';
import SeriesList from '../SeriesList';
import Toast from "../Toast";
import './Landing.css';

class Landing extends Component {

    constructor(props) {
        super(props);
        const localStorageSeries = JSON.parse(localStorage.getItem('series'));
        this.state = {
            series: Array.isArray(localStorageSeries) ? localStorageSeries : [], // { id: number, name: string }
            toast: null
        };
        this.onNewSeries = this.onNewSeries.bind(this);
        this.onDeleteSeries = this.onDeleteSeries.bind(this);
        this.hideMessage = this.hideMessage.bind(this);
        this.showMessage = this.showMessage.bind(this);
    }

    setSeriesOnLocalStorage(series) {
        localStorage.setItem('series', JSON.stringify(series));
    }

    onNewSeries(name, id) {
        if( (this.state.series.filter(series => (series.id === id))).length === 0 ) {
            const series = [ ...this.state.series, { id, name }];
            this.setSeriesOnLocalStorage(series);
            this.setState({ series, toast: { message: 'Serie agregada correctamente', positive: true } });
        } else {
            console.log('Negative message');
            this.setState({ toast: { message: 'La serie ya existe', positive: false } });
        }
    }

    showMessage(message = '', positive = true ) {
        this.setState({ toast: {message, positive} });
    }

    onDeleteSeries(seriesId) {
        const series = this.state.series.filter( ({id}) => id !== seriesId );
        this.setSeriesOnLocalStorage(series);
        this.setState({ series, toast: { message: 'Serie borrada correctamente', positive: true } });
    }

    hideMessage() {
        this.setState({ toast: null });
    }

    renderToast() {
        if( this.state.toast !== null ){
            return (
                <div className="my-series-title-element">
                    <Toast toast={ this.state.toast } hideMessage={this.hideMessage}/>
                </div>
            );
        }

        return "";
    }

    render() {
        return (
            <div className="Landing">
                <AddSeriesForm onNewSeries={this.onNewSeries} showMessage={this.showMessage} />
                <div className="list-title-container">
                    <p className="my-series-title my-series-title-element">Mis series</p> { this.renderToast() }
                </div>
                <SeriesList series={this.state.series} onDeleteSeries={this.onDeleteSeries}/>
            </div>
        );
    }
}

export default Landing;
