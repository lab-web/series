import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SeriesPage.css';
import Header from '../Header';
import ActorCatalogue from '../ActorCatalogue';

class SeriesPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null
        };
    }

    componentWillMount() {
        const localStorageSeries = JSON.parse(localStorage.getItem('series'));
        if(Array.isArray(localStorageSeries)) {
            const seriesFiltered = localStorageSeries.filter(series => (series.id === parseInt(this.props.match.params.id) ));
            if(seriesFiltered.length > 0) {
                this.setState({ title: seriesFiltered[0].name });
            }
        }
    }

    render() {
        const { title } = this.state;
        const seriesId = this.props.match.params.id;
        return (
            <div>
                <Header title={ title } />
                <ActorCatalogue id={ seriesId } />
            </div>
        );
    }
}

SeriesPage.defaultProps = {
    match: {
        params: {
            id: '1'
        }
    }
};

SeriesPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
};

export default SeriesPage;